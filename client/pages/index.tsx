import * as React from "react";
import { NextStatelessComponent } from "next";
import Link from "next/link";
import { Models } from "@converge/api";
import { apiClient } from "@converge/api/client";
import { Layout } from "@converge/design/components";

interface Props {
  posts: Models.Post[];
}

const Index: NextStatelessComponent<Props> = ({ posts }) => {
  return (
    <Layout>
      <div>
        <h1>Index</h1>
        <ul>
          {posts.map(post => (
            <li key={post.id}>
              <Link passHref href={`/${post.id}`}>
                <a>{post.title}</a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </Layout>
  );
};

Index.getInitialProps = async () => {
  const posts = await apiClient.posts.getListing();
  return { posts };
};

export default Index;
