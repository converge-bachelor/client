const path = require("path");
const withTypeScript = require("@zeit/next-typescript");
const withCustomBabelConfigFile = require("next-plugin-custom-babel-config");
const withTranspileModules = require("next-plugin-transpile-modules");

module.exports = withCustomBabelConfigFile(
  withTranspileModules(
    withTypeScript({
      babelConfigFile: path.resolve("../babel.config.js"),
      transpileModules: ["@converge"]
    })
  )
);
